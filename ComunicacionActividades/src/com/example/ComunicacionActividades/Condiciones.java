package com.example.ComunicacionActividades;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * com.example.ComunicacionActividades in ComunicacionActividades project
 * Created by oscar on 18/03/14.
 */
public class Condiciones extends Activity {

    private Button acceptBtn;
    private Button refuseBtn;
    private TextView saludo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conditions);

        saludo = (TextView) findViewById(R.id.saludo);
        Bundle extras = getIntent().getExtras();
        String textoSaludo = "Hola "+ extras.getString("nombre") + ", ¿Aceptas las condiciones?";
        saludo.setText(textoSaludo);

        acceptBtn = (Button) findViewById(R.id.acceptButton);
        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAcceptOrRefuseText(null, (String) acceptBtn.getText());
            }
        });
        refuseBtn =  (Button) findViewById(R.id.refuseButton);
        refuseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAcceptOrRefuseText(null, (String) refuseBtn.getText());
            }
        });
    }

    public void getAcceptOrRefuseText(View view, String text) {
        Intent i = new Intent(this, MyActivity.class);
        i.putExtra("resultado", text);
        setResult(RESULT_OK, i);
        finish();
    }

}
