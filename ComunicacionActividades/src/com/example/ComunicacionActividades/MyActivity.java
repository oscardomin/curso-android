package com.example.ComunicacionActividades;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    private EditText nombreInput;
    private Button verificarBtn;
    private TextView resultado;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        verificarBtn = (Button) findViewById(R.id.verificarButton);
        verificarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lanzarCondiciones(null);
            }
        });
    }

    public void lanzarCondiciones(View view) {
        Intent intent = new Intent(this, Condiciones.class);
        nombreInput = (EditText) findViewById(R.id.nombreInput);
        intent.putExtra("nombre", nombreInput.getText().toString());
        startActivityForResult(intent, 1234);
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode==1234 && resultCode==RESULT_OK) {
            String result = data.getExtras().getString("resultado");
            resultado = (TextView) findViewById(R.id.resultView);
            resultado.setText("Resultado "+ result);
        }
    }

}
