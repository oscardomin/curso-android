import sun.security.jca.GetInstance;

/**
 * Created by oscar on 18/02/14.
 */
public class Complejo {

    private double modulo;
    private double fase;
    private double real;
    private double imaginario;

    public double getModulo() {
        return modulo;
    }

    public void setModulo(double modulo) {
        this.modulo = modulo;
    }

    public double getFase() {
        return fase;
    }

    public void setFase(double fase) {
        this.fase = fase;
    }

    /** Crea un número complejo a partir de su parte real y su parte imaginaria.
     * @param real la parte real del numero complejo que se crea
     * @param imaginario la parte imaginaria del numero complejo que se crea
     */
    public Complejo(double real, double imaginario) {
        setReal(real);
        setImaginario(imaginario);
        setFase(Math.atan(imaginario / real));
        setModulo(real/Math.cos(getFase()));
    }

    public double getReal() {
        return this.real;
    }

    public void setReal(double real) {
        this.real = real;
        actualizaModuloYFase();
    }

    public double getImaginario() {
        return this.imaginario;
    }

    public void setImaginario(double imaginario) {
        this.imaginario = imaginario;
        actualizaModuloYFase();
    }

    public void actualizaModuloYFase() {
        setFase(Math.atan(getImaginario() / getReal()));
        setModulo(getReal()/Math.cos(getFase()));
    }

    /** Retorna un String con la parte real e imaginaria del numero complejo
     * @return  String con la parte real e imaginaria separados por un simbolo de suma y la parte imaginaria con una i al final.
     */
    public String toString() {
        return getReal() + "+" + getImaginario() + "i";
    }

    /** Suma al complejo de este objeto otro complejo.
     * @param v el complejo que sumamos
     */
    public void suma (Complejo v) {
        setReal(this.getReal() + v.getReal());
        setImaginario(this.getImaginario() + v.getImaginario());
        actualizaModuloYFase();
    }

    public void suma (double re, double im) {
        setReal(getReal() + re);
        setImaginario(getImaginario() + im);
        actualizaModuloYFase();
    }

    public void suma (double re) {
        setReal(getReal() + re);
        actualizaModuloYFase();
    }
}
