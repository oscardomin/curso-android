/**
 * PACKAGE_NAME in ejemploJava project
 * Created by oscar on 18/02/14.
 */
public class ComplejoAmpliado extends Complejo {

    private boolean esReal;

    public ComplejoAmpliado(double re, double im) {
        super(re, im);
        esReal = im == 0;
    }

    public ComplejoAmpliado(double re){
        super(re, 0);
        esReal = true;
    }

    @Override public void suma (Complejo c) {
        esReal = getImaginario() == -c.getImaginario();
        super.suma(c);
    }

    public String toString() {
        if(esReal()) {
            return getReal() + " ¡real!";
        } else {
            return super.toString();
        }
    }

    public boolean esReal() {
        return esReal;
    }
}
