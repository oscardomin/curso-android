package com.example.Intenciones;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyActivity extends Activity {
	/**
	 * Called when the activity is first created.
	 */

	private Button webBtn;
	private Button callBtn;
	private Button mapsBtn;
	private Button cameraBtn;
	private Button mailBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		webBtn = (Button) findViewById(R.id.webButton);
		webBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				pgWeb(null);
			}
		});

		callBtn = (Button) findViewById(R.id.phoneButton);
		callBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				llamadaTelefono(null);
			}
		});

		mapsBtn = (Button) findViewById(R.id.mapsButton);
		mapsBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				googleMaps(null);
			}
		});

		cameraBtn = (Button) findViewById(R.id.cameraButton);
		cameraBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				tomarFoto(null);
			}
		});

		mailBtn = (Button) findViewById(R.id.mailButton);
		mailBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mandarCorreo(null);
			}
		});
	}

	public void pgWeb(View view) {
		Intent intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("http://www.androidcurso.com/"));
		startActivity(intent);
	}

	public void llamadaTelefono(View view) {
		Intent intent = new Intent(Intent.ACTION_DIAL,
				Uri.parse("tel:962849347"));
		startActivity(intent);
	}

	public void googleMaps(View view) {
		Intent intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("google.streetview:cbll=41.656313,-0.877351"));
		startActivity(intent);
	}

	public void tomarFoto(View view) {
		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
		startActivity(intent);
	}

	public void mandarCorreo(View view) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, "asunto");
		intent.putExtra(Intent.EXTRA_TEXT, "texto del correo");
		intent.putExtra(Intent.EXTRA_EMAIL,
				new String[] {"jtomas@upv.es" });
		startActivity(intent);
	}
}
