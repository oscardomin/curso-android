package com.example.MasVistas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MasVistasActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    private EditText entrada;
    private TextView salida;
    private Button bAcercaDe;
    private Button salirButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        entrada = (EditText) findViewById(R.id.entrada);
        salida = (TextView)  findViewById(R.id.salida);
        bAcercaDe = (Button) findViewById(R.id.aboutbtn);
        salirButton = (Button) findViewById(R.id.salirbtn);
        bAcercaDe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lanzarAcercaDe(null);
            }
        });
        salirButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void sePulsa(View view){
        salida.setText(String.valueOf(Float.parseFloat(entrada.getText().toString())*2.0));
    }

    public void sePulsa0(View view){
        entrada.setText(entrada.getText()+(String)view.getTag());
    }

    public void lanzarAcercaDe(View view){
        Intent i = new Intent(this, AcercaDe.class);
        startActivity(i);
    }
}
