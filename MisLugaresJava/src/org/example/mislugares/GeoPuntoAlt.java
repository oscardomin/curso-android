package org.example.mislugares;

/**
 * org.example.mislugares in MisLugaresJava project
 * Created by oscar on 18/02/14.
 */
public class GeoPuntoAlt extends GeoPunto {

    private int altura;

    public GeoPuntoAlt(double latitud, double longitud, int altura) {
        super(latitud, longitud);
        this.altura = altura;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    @Override
    public String toString() {
        return "GeoPuntoAlt{" +
                "altura=" + altura +
                '}';
    }

    public double distancia(GeoPuntoAlt gpa) {
        GeoPunto gp2 = new GeoPunto(gpa.getLatitud(), gpa.getLongitud());
        GeoPunto gp1 = new GeoPunto(getLatitud(), getLongitud());
        double distancia_anterior = gp1.distancia(gp2);
        double distancia_nueva = Math.sqrt(distancia_anterior*distancia_anterior + altura-gpa.altura);
        return distancia_nueva;
    }
}
