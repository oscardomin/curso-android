package org.example.MisLugares;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * org.example.MisLugares in MisLugares project
 * Created by oscar on 19/03/14.
 */
public class Preferencias extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        addPreferencesFromResource(R.xml.preferencias);
    }
}
