package org.example.MisLugares;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

public class MainActivity extends FragmentActivity implements LocationListener {
	/**
	 * Called when the activity is first created.
	 */

	//public BaseAdapter adaptor;
	private MediaPlayer mp;

	private LocationManager manejador;
	private Location mejorLocaliz;

	private static final long DOS_MINUTOS = 2 * 60 * 1000;

	private VistaLugarFragment fragmentVista;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		//adaptor = new MiAdaptador(this);

		fragmentVista = (VistaLugarFragment) getSupportFragmentManager()
				.findFragmentById(R.id.vista_lugar_fragment);

		Lugares.indicializaBD(this);
		/*adaptor = new SimpleCursorAdapter(this,
				R.layout.elemento_lista,
				Lugares.listado(),
				new String[] { "nombre", "direccion"},
				new int[] { R.id.nombre, R.id.direccion});
         */
		//adaptor = new AdaptadorCursorLugares(this, Lugares.listado());
		//setListAdapter(adaptor);

		Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();

		mp = MediaPlayer.create(this, R.raw.audio);
		//mp.start();
		mp.stop();

		manejador = (LocationManager) getSystemService(LOCATION_SERVICE);

		if(manejador.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			actualizaMejorLocaliz(manejador.getLastKnownLocation(
					LocationManager.GPS_PROVIDER));
		}

		if(manejador.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			actualizaMejorLocaliz(manejador.getLastKnownLocation(
					LocationManager.NETWORK_PROVIDER));
		}

	}

	@Override protected void onStart() {
		super.onStart();
		//mp.start();
		mp.stop();
		Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
	}

	@Override protected void onResume() {
		super.onResume();
		//mp.start();
		mp.stop();
		activarProveedores();
		Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
	}

	private void activarProveedores() {
		if(manejador.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			manejador.requestLocationUpdates(LocationManager.GPS_PROVIDER,
					20 * 1000, 5, this);
		}

		if(manejador.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			manejador.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
					10 * 1000, 10, this);
		}
	}

	@Override public void onLocationChanged(Location location) {
		Log.d(Lugares.TAG, "Nueva localización: " + location);
		actualizaMejorLocaliz(location);
	}


	@Override public void onProviderDisabled(String proveedor) {
		Log.d(Lugares.TAG, "Se deshabilita: "+proveedor);
		activarProveedores();
	}

	@Override    public void onProviderEnabled(String proveedor) {
		Log.d(Lugares.TAG, "Se habilita: "+proveedor);
		activarProveedores();
	}

	@Override
	public void onStatusChanged(String proveedor, int estado, Bundle extras) {
		Log.d(Lugares.TAG, "Cambia estado: "+proveedor);
		activarProveedores();
	}

	private void actualizaMejorLocaliz(Location localiz) {
		if (mejorLocaliz == null
				|| localiz.getAccuracy() < 2*mejorLocaliz.getAccuracy()
				|| localiz.getTime() - mejorLocaliz.getTime() > DOS_MINUTOS) {
			Log.d(Lugares.TAG, "Nueva mejor localización");
			mejorLocaliz = localiz;
			if(localiz != null) {
				Lugares.posicionActual.setLatitud((int)localiz.getLatitude());
				Lugares.posicionActual.setLongitud((int)localiz.getLongitude());
			}
		}
	}

	@Override protected void onPause() {
		Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
		//mp.start();
		mp.stop();
		super.onPause();
		manejador.removeUpdates(this);
	}

	@Override protected void onStop() {
		super.onStop();
		//mp.start();
		mp.stop();
		Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
	}

	@Override protected void onRestart() {
		super.onRestart();
		//mp.start();
		mp.stop();
		Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show();
	}

	@Override protected void onDestroy() {
		super.onDestroy();
		Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onSaveInstanceState(Bundle estadoGuardado) {
		super.onSaveInstanceState(estadoGuardado);
		if (mp != null) {
			int pos = mp.getCurrentPosition();
			estadoGuardado.putInt("posicion", pos);
		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle estadoGuardado){
		super.onRestoreInstanceState(estadoGuardado);
		if (estadoGuardado != null && mp != null) {
			int pos = estadoGuardado.getInt("posicion");
			mp.seekTo(pos);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true; // true -> el menu ya esta visible
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.config:
				lanzarConfiguracion(null);
				break;
			case R.id.acercaDe:
				lanzarAcercaDe(null);
				break;
			case R.id.menu_buscar:
				lanzarBuscador(null);
				break;
			case R.id.menu_mapa:
				Intent i = new Intent(this, Mapa.class);
				startActivityForResult(i, 0);
				break;
			case R.id.accion_nuevo:
				long id = Lugares.nuevo();
				Intent intent = new Intent(this, EdicionLugar.class);
				intent.putExtra("id", id);
				startActivityForResult(intent, 0);
				return true;
			default:
				break;
		}
		return super.onOptionsItemSelected(item); // true -> consumimos el item, no se propaga
	}

	public void lanzarAcercaDe(View view) {
		Intent intent = new Intent(this, AcercaDe.class);
		startActivity(intent);
	}

	public void lanzarConfiguracion(View view) {
		Intent intent = new Intent(this, Preferencias.class);
		startActivity(intent);
	}

	public void lanzarBuscador(View view) {
		//TODO: implementar actividad lanzarBuscador
	}

	public void mostrarPreferencias(View view) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		String s = "notificaciones: " + pref.getBoolean("notificaciones", true)
				+ ", distancia mínima: " + pref.getString("distancia", "?");

		Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

		String sMail = "notificaciones_correo: " + pref.getBoolean("notificaciones_correo", true)
				+ "direccion de correo: " + pref.getString("email", "?")
				+ "\ntipo de notificación: " + pref.getString("tipos", "?");


		Toast.makeText(this, sMail, Toast.LENGTH_SHORT).show();
	}
	/* Fragmento de código en desuso debido a cambios en las vistas: De botones a ListView
	public void lanzarVistaLugar(View view) {
		final EditText entrada = new EditText(this);
		entrada.setText("0");
		new AlertDialog.Builder(this)
				.setTitle("Selección de lugar")
				.setMessage("indica su id:")
				.setView(entrada)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						long id = Long.parseLong(entrada.getText().toString());
						Intent i = new Intent(MainActivity.this, VistaLugar.class);
						i.putExtra("id", id);
						startActivity(i);
					}
				})
				.setNegativeButton("Cancelar", null)
				.show();
	} */

	/*@Override
	protected void onListItemClick(ListView listView, View vista, int posicion, long id) {
		super.onListItemClick(listView, vista, posicion, id);
		Intent intent = new Intent(this, VistaLugar.class);
		intent.putExtra("id", id);
		startActivityForResult(intent, 0);
	} */

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ListView listView = (ListView) findViewById(R.id.listView);
		AdaptadorCursorLugares adaptador =(AdaptadorCursorLugares)                                   listView.getAdapter();
		adaptador.changeCursor(Lugares.listado());
	}

	public void muestraLugar(long id) {
		if (fragmentVista != null) {
			fragmentVista.actualizarVistas(id);
		} else {
			Intent intent = new Intent(this, VistaLugar.class);
			intent.putExtra("id", id);
			startActivityForResult(intent, 0);
		}
	}
}
