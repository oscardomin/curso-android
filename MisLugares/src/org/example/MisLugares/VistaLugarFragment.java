package org.example.MisLugares;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

/**
 * org.example.MisLugares in MisLugares project
 * Created by oscar on 19/03/14.
 */
public class VistaLugarFragment extends Fragment {

	private long id;
	private Lugar lugar;
	private Uri uriFoto;

	final static int RESULTADO_EDITAR = 1;
	final static int RESULTADO_GALERIA = 2;
	final static int RESULTADO_FOTO = 3;

	@Override
	public View onCreateView(LayoutInflater inflador, ViewGroup contenedor,
	                         Bundle savedInstanceState) {
		View vista = inflador.inflate(R.layout.vista_lugar, contenedor, false);
		setHasOptionsMenu(true);

		LinearLayout pUrl = (LinearLayout) vista.findViewById(R.id.p_url);
		pUrl.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				pgWeb(null);
			}
		});

		return vista;
	}

	@Override
	public void onActivityCreated(Bundle state) {
		super.onActivityCreated(state);
		Bundle extras = getActivity().getIntent().getExtras();
		if(extras != null) {
			id = extras.getLong("id", -1);
			if(id != -1) {
				actualizarVistas(id);
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.vista_lugar, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.accion_compartir:
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT,
						lugar.getNombre() + " - " + lugar.getUrl());
				startActivity(intent);
				return true;
			case R.id.accion_llegar:
				verMapa(null);
				return true;
			case R.id.accion_editar:
				editaLugar(null);
				return true;
			case R.id.accion_borrar:
				borrarLugar(null);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void borrarLugar(View view) {
		new AlertDialog.Builder(getActivity())
				.setTitle("Borrado de lugar")
				.setMessage("¿Estás seguro que quieres eliminar este lugar?")
				.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Lugares.borrar((int) id);
						getActivity().finish();
					}
				})
				.setNegativeButton("Cancelar", null)
				.show();
	}

	public void editaLugar(View view) {
		Intent i = new Intent(getActivity(), EdicionLugar.class);
		i.putExtra("id", id);
		startActivityForResult(i, RESULTADO_EDITAR);
	}

	public void actualizarVistas(final long id) {
		this.id = id;
		lugar= Lugares.elemento((int) id);
		if(lugar != null) {
			View v = getView();
			Lugares.actualizaLugar((int) id, lugar);
			TextView nombre = (TextView) v.findViewById(R.id.nombre);
			nombre.setText(lugar.getNombre());

			ImageView logo_tipo = (ImageView) v.findViewById(R.id.logo_tipo);
			logo_tipo.setImageResource(lugar.getTipo().getRecurso());

			TextView tipo = (TextView) v.findViewById(R.id.tipo);
			tipo.setText(lugar.getTipo().getTexto());

			//ImageView imageView = (ImageView) findViewById(R.id.foto);
			ponerFoto((ImageView)v.findViewById(R.id.foto), lugar.getFoto());

			if (lugar.getDireccion().length() == 0) {
				v.findViewById(R.id.direccion).setVisibility(View.GONE);
			} else {
				TextView direccion = (TextView) v.findViewById(R.id.direccion);
				direccion.setText(lugar.getDireccion());
			}

			if (lugar.getTelefono() == 0) {
				v.findViewById(R.id.telefono).setVisibility(View.GONE);
			} else {
				TextView telefono = (TextView) v.findViewById(R.id.telefono);
				telefono.setText(Integer.toString(lugar.getTelefono()));
			}

			if (lugar.getUrl().length() == 0) {
				v.findViewById(R.id.url).setVisibility(View.GONE);
			} else {
				TextView url = (TextView) v.findViewById(R.id.url);
				url.setText(lugar.getUrl());
			}

			if (lugar.getComentario().length() == 0) {
				v.findViewById(R.id.comentario).setVisibility(View.GONE);
			} else {
				TextView comentario = (TextView) v.findViewById(R.id.comentario);
				comentario.setText(lugar.getComentario());
			}

			TextView fecha = (TextView) v.findViewById(R.id.fecha);
			fecha.setText(DateFormat.getDateInstance().format(
					new Date(lugar.getFecha())));

			TextView hora = (TextView) v.findViewById(R.id.hora);
			hora.setText(DateFormat.getTimeInstance().format(
					new Date(lugar.getFecha())));

			RatingBar valoracion = (RatingBar) v.findViewById(R.id.valoracion);
			valoracion.setOnRatingBarChangeListener(null);
			valoracion.setRating(lugar.getValoracion());
			valoracion.setOnRatingBarChangeListener(
					new RatingBar.OnRatingBarChangeListener() {
						@Override
						public void onRatingChanged(RatingBar ratingBar,
						                            float valor, boolean fromUser) {
							lugar.setValoracion(valor);
						}
					}
			);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RESULTADO_EDITAR) {
			actualizarVistas(id);
			getView().findViewById(R.id.scrollView1).invalidate();
		} else if (requestCode == RESULTADO_GALERIA && resultCode == Activity.RESULT_OK) {
			lugar.setFoto(data.getDataString());
			//ImageView imageView = (ImageView) findViewById(R.id.foto);
			Lugares.actualizaLugar((int) id, lugar);
			ponerFoto((ImageView)getView().findViewById(R.id.foto), lugar.getFoto());
		}
		else if(requestCode == RESULTADO_FOTO && resultCode == Activity.RESULT_OK
		&& lugar!=null && uriFoto!=null) {
			lugar.setFoto(uriFoto.toString());
			//ImageView imageView = (ImageView) findViewById(R.id.foto);
			Lugares.actualizaLugar((int) id, lugar);
			ponerFoto((ImageView)getView().findViewById(R.id.foto), lugar.getFoto());
		}
	}

	public void verMapa(View view) {
		Uri uri;
		double lat = lugar.getPosicion().getLatitud();
		double lon = lugar.getPosicion().getLongitud();
		if (lat != 0 || lon != 0) {
			uri = Uri.parse("geo:" + lat + "," + lon);
		} else {
			uri = Uri.parse("geo:0,0?q=" + lugar.getDireccion());
		}
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(intent);
	}

	public void llamadaTelefono(View view) {
		startActivity(new Intent(Intent.ACTION_DIAL,
				Uri.parse("tel:" + lugar.getTelefono())));
	}


	public void pgWeb(View view) {
		startActivity(new Intent(Intent.ACTION_VIEW,
				Uri.parse(lugar.getUrl())));
	}

	protected void ponerFoto(ImageView imageView, String uri) {
		if (uri != null) {
			imageView.setImageURI(Uri.parse(uri));
		} else{
			imageView.setImageBitmap(null);
		}
	}

	public void galeria(View view) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("image/*");
		startActivityForResult(intent, RESULTADO_GALERIA);
	}

	public void tomarFoto(View view) {
		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
		uriFoto = Uri.fromFile(
				new File(Environment.getExternalStorageDirectory() + File.separator
						+ "img_" + (System.currentTimeMillis() / 1000) + ".jpg"));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uriFoto);
		startActivityForResult(intent, RESULTADO_FOTO);
	}

	public void eliminarFoto(View view) {
		new AlertDialog.Builder(getActivity())
				.setTitle("Borrar fotografia")
				.setMessage("¿Estás seguro que quieres quitar la foto de este lugar?")
				.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						lugar.setFoto(null);
						ponerFoto((ImageView)getView().findViewById(R.id.foto), null);
						//finish();
					}
				})
				.setNegativeButton("Cancelar", null)
				.show();
		Lugares.actualizaLugar((int) id, lugar);
	}
}
