package org.example.MisLugares;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import java.text.DateFormat;
import java.util.Date;

/**
 * org.example.MisLugares in MisLugares project
 * Created by oscar on 19/03/14.
 */
public class EdicionLugar extends Activity {

	private long id;
	private Lugar lugar;
	private EditText nombre;
	private Spinner tipo;
	private EditText direccion;
	private EditText telefono;
	private EditText url;
	private EditText comentario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edicion_lugar);

		Bundle extras = getIntent().getExtras();
		id = extras.getLong("id", -1);
		lugar = Lugares.elemento((int) id);

		nombre = (EditText) findViewById(R.id.editName);
		nombre.setText(lugar.getNombre());

		tipo = (Spinner) findViewById(R.id.spinnerType);
		ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, TipoLugar.getNombres());
		adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tipo.setAdapter(adaptador);
		tipo.setSelection(lugar.getTipo().ordinal());

		direccion = (EditText) findViewById(R.id.editAddress);
		direccion.setText(lugar.getDireccion());

		telefono = (EditText) findViewById(R.id.editPhone);
		telefono.setText(Integer.toString(lugar.getTelefono()));

		url = (EditText) findViewById(R.id.editURL);
		url.setText(lugar.getUrl());

		comentario = (EditText) findViewById(R.id.editComment);
		comentario.setText(lugar.getComentario());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.editar_lugar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.accion_guardar:
				guardarEdicion();
				return true;
			case R.id.accion_cancelar:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void guardarEdicion() {
		lugar.setNombre(nombre.getText().toString());
		lugar.setTipo(TipoLugar.values()[tipo.getSelectedItemPosition()]);
		lugar.setDireccion(direccion.getText().toString());
		lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
		lugar.setUrl(url.getText().toString());
		lugar.setComentario(comentario.getText().toString());
		Lugares.actualizaLugar((int) id, lugar);
		finish();
	}
}
