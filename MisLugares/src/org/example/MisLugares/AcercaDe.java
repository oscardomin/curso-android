package org.example.MisLugares;

import android.app.Activity;
import android.os.Bundle;

/**
 * org.example.MisLugares in MisLugares project
 * Created by oscar on 18/03/14.
 */
public class AcercaDe extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
    }
}
