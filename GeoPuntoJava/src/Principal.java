/**
 * Created by oscar on 18/02/14.
 */
public class Principal {
    public static void main(String[] main) {
        GeoPunto gp1 = new GeoPunto(1.2, 7.4);
        GeoPunto gp2 = new GeoPunto(1.2, 7.2);

        System.out.println("Punto1: " + gp1.toString() + "\nPunto2: " + gp2.toString() + "\ndistancia: " + gp1.distancia(gp2));
    }
}
