/**
 * Created by oscar on 18/02/14.
 */
public class GeoPunto {
    private int longitud;
    private int latitud;

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public int getLatitud() {
        return latitud;
    }

    public void setLatitud(int latitud) {
        this.latitud = latitud;
    }

    public GeoPunto(double latitud, double longitud) {
        setLongitud((int)(longitud * 1E6));
        setLatitud((int)(latitud * 1E6));
    }

    public String toString() {
        return getLongitud() + ", " + getLatitud();
    }

    public double distancia(GeoPunto gp) {
        final double RADIO_TIERRA = 6378000; // en metros
        double difLat = Math.toRadians(getLatitud() - gp.getLatitud());
        double difLong = Math.toRadians(getLongitud() - gp.getLongitud());
        double sinusLat = Math.sin(difLat/2);
        double sinusLong = Math.sin(difLong/2);
        double a = sinusLat*sinusLat + Math.cos(Math.toRadians(getLatitud()))*Math.cos(Math.toRadians(gp.getLatitud()))*sinusLong*sinusLong;
        double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return RADIO_TIERRA*c;
    }
}
